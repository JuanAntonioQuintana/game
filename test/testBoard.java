import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class testBoard {

    private String[][] field;

    @Before
    public void setUp(){
        field = new String[][]{{"E","E","D","E"},
                               {"O","O","M","E"},
                               {"K","E","E","E"},
                               {"E","O","E","E"},
                               {"E","O","E","C"}};
    }

    @Test
    public void character_move_one_position_forward() {
        Board board = new Board(field);
        board.moveForward();
        assertEquals(board.getCharacterPosition(), new Position(3, 3));
    }

    @Test
    public void character_move_two_position_forward(){
        Board board = new Board(field);
        board.moveForward();
        board.moveForward();
        assertEquals(board.getCharacterPosition(), new Position(2,3));
    }
}
