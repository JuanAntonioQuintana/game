
public class Board {
    private String[][] field;

    public Board(String[][] field) {
        this.field = new String[field.length][field.length];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                this.field[i][j] = field[i][j];
            }
        }
    }

    public Position getCharacterPosition() {
        for (int i = 0; i < this.field.length; i++) {
            for (int j = 0; j < this.field[i].length; j++) {
                if(field[i][j] == "C"){
                    return new Position(i,j);
                }
            }
        }
        return null;
    }

    public void moveForward() {
        Position position = getCharacterPosition();
        if(field[position.getX()-1][position.getY()] != "O"){
            field[position.getX()-1][position.getY()] = "C";
            field[position.getX()][position.getY()] = "E";
        }
    }
}
